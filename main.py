"""
Calculatrice using Kivy

This Kivy application creates a simple calculator with buttons for digits 0-9, decimal point, and operations +, -, *, /, and =.

The layout consists of a FloatLayout containing a Label for displaying the result and a GridLayout for arranging the buttons.

"""

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout


class Calculatrice(App):

    def build(self):
        # Initialize expression to store user input
        self.expression = ""

        # Create a FloatLayout to manage the overall layout
        layout = FloatLayout(size=(400, 600))

        # Result label at the top of the layout
        self.result_label = Label(text="0", font_size='20sp', size_hint=(1, 0.2), pos_hint={'top': 1})
        layout.add_widget(self.result_label)

        # Button labels for the calculator
        buttons = [
            '7', '8', '9', '/',
            '4', '5', '6', '*',
            '1', '2', '3', '-',
            '.', '0', '=', '+'
        ]

        # GridLayout to arrange the buttons in a grid
        button_grid = GridLayout(cols=4, spacing=10, size_hint=(1, 0.8), pos_hint={'top': 0.8})

        # Create buttons and add them to the GridLayout
        for button_text in buttons:
            button = Button(text=button_text, on_press=self.on_button_press, size_hint=(1, 1))
            button_grid.add_widget(button)

        # Add the button grid to the FloatLayout
        layout.add_widget(button_grid)

        return layout

    def on_button_press(self, instance):
        # Handle button press event
        button_text = instance.text

        if button_text == '=':
            # Evaluate the expression when '=' is pressed
            try:
                result = str(eval(self.expression))
                self.expression = result
            except Exception as e:
                result = "Error"
                self.expression = ""
        else:
            # Append the button text to the expression
            self.expression += button_text

        # Update the result label with the current expression
        self.result_label.text = self.expression


if __name__ == '__main__':
    # Run the Calculatrice
    Calculatrice().run()
